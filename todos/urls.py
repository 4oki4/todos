from django.urls import path, include
from .views import ListTodo
from .views import DetailTodo

urlpatterns = [
    path('<int:pk>/', DetailTodo.as_view()),
    path('', ListTodo.as_view()),
]